# tout est dans le titre
from os import listdir
from PIL import Image

images_name_mouse = [f for f in listdir("../images/mouse")]
images_name_nose = [f for f in listdir("../images/nose")]
images_name_eyes = [f for f in listdir("../images/eyes")]

size_eyes = []
size_nose = []
size_mouse = []

for i in range (len(images_name_eyes)):
  image = Image.open('../images/eyes/'+images_name_eyes[i])
  if (image.size not in size_eyes):
    size_eyes.append(image.size)
  
  image = Image.open('../images/nose/'+images_name_nose[i])
  if (image.size not in size_nose):
    size_nose.append(image.size)
  
  image = Image.open('../images/mouse/'+images_name_mouse[i])
  if (image.size not in size_mouse):
    size_mouse.append(image.size)

if ( (len(size_eyes) + len(size_nose) + len(size_mouse) ) != 3):
  print ("toutes les images ne font pas la même taille !")
  print ("taille des images yeux : ")
  print (size_eyes)
  print ("taille des images nez :")
  print (size_nose)
  print ("taille des images bouche :")
  print (size_mouse)
else :
  print ("ok")