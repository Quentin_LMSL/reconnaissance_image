# avec un fichier contenant les id des célébrités (le fichier id_30occ_atLeast)
# Copie toutes leurs photo dans un dossier "img_align_30_occur"
dir_dest = "img_align_30_occur/"
dir_src = "img_align_celeba/"
#on prend les 50 premier du fichier
nb_ids_to_take = 50

import shutil

file = open("id_30occ_atLeast.txt", "r")
lines = file.read().splitlines()

# tous nos id
ids = [line for line in lines[:nb_ids_to_take]]

file = open("identity_CelebA.txt", "r")
lines = file.readlines()

for line in lines:
  # attention au dernier caracère qui un espace qui fausse tout
  id_line = line.split(" ")[1][:-1]
  filename = line.split(" ")[0]
  if (id_line in ids):
    shutil.copyfile(dir_src+filename, dir_dest+filename)