# sépare les données du fichier nom_image-id_celeb.txt en deux fichier
# le fichier nom_image-id_celeb_training référençant les images pour le training
# et de même façon nom_image-id_celeb_validation pour la validation

# étant donné que nous avons 30 images de 50 célébrités on à 1500 images de leurs nezs,oeils et bouches
# nous prendrons donc 24 images de chaque pour l'entrainement car 30*0.8 = 24, on a de la chance ça tombe pile.

n = 24

file = open("../../infoTxt/nom_image-id_celeb.txt", "r")
lines = file.read().splitlines()

training_data = []
validation_data = []

i = 0
for line in lines :
  if (i < 24) :
    training_data.append(line)
    i = i + 1
  else :
    validation_data.append(line)
    i = i + 1
  if (i == 30) :
    i = 0

fichier = open("../../infoTxt/nom_image-id_celeb_training.txt", "a")
for data in training_data :
  fichier.write(data+"\n")
fichier.close()

fichier = open("../../infoTxt/nom_image-id_celeb_validation.txt", "a")
for data in validation_data :
  fichier.write(data+"\n")
fichier.close()