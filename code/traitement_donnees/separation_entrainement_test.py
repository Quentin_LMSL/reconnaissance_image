# séparera nos images cropp en deux ensemble
# 1 - l'ensemble d'entrainement qui contiendra 80% des éléments
# 2 - l'ensemble de validation qui contiendra 20% des éléments
# étant donné que nous avons 30 images de 50 célébrités on à 1500 images de leurs nezs,oeils et bouches
# nous prendrons donc 24 images de chaque pour l'entrainement car 30*0.8 = 24, on a de la chance ça tombe pile.

import shutil


n = 24

file = open("../infoTxt/50_id_nbOcc_nClass.txt", "r")
lines = file.read().splitlines()
# suppression de l'entête
lines = lines[1:]
# on isole nos id
ids = [id.split(" ")[0] for id in lines]


# on recoupe avec le fichier identity_CelebA.txt pour connaître à qui appartient chaque image
# on va en profiter pour créer un fichier avec ces données avec un print

# init de notre dictionnaire
dict_ids_imageName = {}
for id in ids:
  dict_ids_imageName[id] = []

file = open("../infoTxt/identity_CelebA.txt", "r")
lines = file.read().splitlines()

for line in lines:
  img_name = line.split(" ")[0]
  id = line.split(" ")[1]

  if (id in dict_ids_imageName):
    dict_ids_imageName[id].append(img_name)

# pour créer le fichier nom_image-id-num_class avec print
# num_class = 0
# for key in dict_ids_imageName:
#   for image_name in dict_ids_imageName[key]:
#     print(image_name+" "+key+" "+str(num_class))
#   num_class = num_class + 1

#copie des images dans les dossier qui vont bien

for key in dict_ids_imageName:
  nb_cp = 0
  for image_name in dict_ids_imageName[key]:
    if (nb_cp < n):
      shutil.copy("../images/eyes/"+image_name, "../images/training/eyes/"+image_name)
      shutil.copy("../images/nose/"+image_name, "../images/training/nose/"+image_name)
      shutil.copy("../images/mouse/"+image_name, "../images/training/mouse/"+image_name)
    else:
      shutil.copy("../images/eyes/"+image_name, "../images/validation/eyes/"+image_name)
      shutil.copy("../images/nose/"+image_name, "../images/validation/nose/"+image_name)
      shutil.copy("../images/mouse/"+image_name, "../images/validation/mouse/"+image_name)
    nb_cp = nb_cp +1