#depuis les images du dossier : img_align_30_occur
#dans 3 dossier : img_align_30_occur_[mouse,nose,eyes]
#nous sauvegarderons les images crop

from os import listdir
from PIL import Image

images_name = [f for f in listdir("../images/img_align_30_occur")]
images_name.sort()

file = open("../infoTxt/list_landmarks_align_celeba.txt", "r")
lines = file.read().splitlines()
indice_image_name = 0

marge_px = 15

for line in lines:
  if (line.split(" ")[0] == images_name[indice_image_name]):

    #on regardera la prochaine image_name si il y en a une
    if (indice_image_name < len(images_name) - 1):
      indice_image_name = indice_image_name + 1

    splited_line_paddingErr = line.split(" ")
    splited_line = [line for line in splited_line_paddingErr if len(line) > 0]

    lefteye_x = int(splited_line[1])
    lefteye_y = int(splited_line[2])
    righteye_x = int(splited_line[3])
    righteye_y = int(splited_line[4])
    nose_x = int(splited_line[5])
    nose_y = int(splited_line[6])
    leftmouth_x = int(splited_line[7])
    leftmouth_y = int(splited_line[8])
    rightmouth_x = int(splited_line[9])
    rightmouth_y = int(splited_line[10])

    # print(indice_image_name)
    # print(splited_line[0])
    # print(lefteye_x,lefteye_y,righteye_x,righteye_y,nose_x,nose_y,leftmouth_x,leftmouth_y,rightmouth_x,rightmouth_y)

    # Opens a image in RGB mode
    image = Image.open('../images/img_align_30_occur/'+splited_line[0])
    # print(f'Dimensions de l\'image originale: {image.size}')
    # cropped_image_eye = image.crop((lefteye_x-marge_px, lefteye_y-marge_px, righteye_x+marge_px, righteye_y+marge_px))
    # cropped_image_nose = image.crop((nose_x-marge_px, nose_y-marge_px*1.3, nose_x+marge_px, nose_y+marge_px*1.3))
    # cropped_image_mouse = image.crop((leftmouth_x-marge_px, leftmouth_y-marge_px, rightmouth_x+marge_px, rightmouth_y+marge_px))
    # avec des constantes pour que toutes les images fassent la même taille :
    cropped_image_eye = image.crop((70-marge_px, 110-marge_px, 108+marge_px, 110+marge_px))
    cropped_image_nose = image.crop((90-marge_px, 120-marge_px*1.3, 90+marge_px, 130+marge_px*1.3))
    cropped_image_mouse = image.crop((70-marge_px, 140-marge_px, 108+marge_px, 155+marge_px))
    # Enregistrez l'image découpée
    cropped_image_eye.save('../images/eyes/'+splited_line[0])
    cropped_image_nose.save('../images/nose/'+splited_line[0])
    cropped_image_mouse.save('../images/mouse/'+splited_line[0])
    # image.show()
    # cropped_image_eye.show()
    # cropped_image_nose.show()
    # cropped_image_mouse.show()
